package com.pbl6.europhiaeureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurophiaEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurophiaEurekaApplication.class, args);
	}

}
