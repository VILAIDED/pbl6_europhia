package com.example.europhiauser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEurekaClient
public class EurophiaUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurophiaUserApplication.class, args);
	}

}
