package com.pbl6.europhiaauth.controllers;

import com.pbl6.europhiaauth.payload.request.SignupRequest;
import com.pbl6.europhiaauth.payload.response.ResponseCustom;
import com.pbl6.europhiaauth.payload.response.ValidateResponse;
import com.pbl6.europhiaauth.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    private final UserService userService;
    @PostMapping("/signup")
    public ResponseEntity register(@RequestBody @Valid SignupRequest req){
        return ResponseEntity.ok(userService.signup(req));
    }
    @PostMapping("/validate")
    public ResponseEntity validatetoken(HttpServletRequest request){
        String username = (String) request.getAttribute("username");
        List<String> grantedAuthorities = (List<String>) request.getAttribute("authorities");
        ValidateResponse res = new ValidateResponse(username,grantedAuthorities);

        return ResponseEntity.ok(res);
    }
    @GetMapping("/admin")
    public ResponseEntity admin() {
        return new ResponseEntity(new ResponseCustom<>("OK", "Hello admin", HttpStatus.OK), HttpStatus.OK);
    }
    @GetMapping("/user")
    public ResponseEntity user(){
        return new ResponseEntity(new ResponseCustom<>("OK", "Hello client", HttpStatus.OK), HttpStatus.OK);

    }

}

