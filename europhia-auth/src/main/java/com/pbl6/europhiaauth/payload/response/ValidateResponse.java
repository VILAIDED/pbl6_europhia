package com.pbl6.europhiaauth.payload.response;

import org.springframework.security.core.GrantedAuthority;

import java.util.List;

public class ValidateResponse {
    private String token;
    private List<String> authorities;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

    public ValidateResponse(String token, List<String> authorities) {
        this.token = token;
        this.authorities = authorities;
    }
}
