package com.pbl6.europhiaauth.security.filters;


import com.pbl6.europhiaauth.exception.CustomException;
import com.pbl6.europhiaauth.security.JwtTokenProvider;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JwtTokenFilter extends OncePerRequestFilter {
    private JwtTokenProvider jwtTokenProvider;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    protected void doFilterInternal(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, javax.servlet.FilterChain filterChain) throws ServletException, IOException {
        String token  = jwtTokenProvider.resolveToken(request);
        try{
            if (token != null && jwtTokenProvider.validateToken(token)){
                Authentication auth = jwtTokenProvider.getAuthentication(token);
                Jws<Claims> authClaim = jwtTokenProvider.getClaim(token);
                String username = authClaim.getBody().getSubject();

                List<Map<String, String>> authorities = (List<Map<String, String>>) authClaim.getBody().get("authorities");
                List<GrantedAuthority> grantedAuthorities = authorities.stream().map(map -> new SimpleGrantedAuthority(map.get("authority")))
                        .collect(Collectors.toList());
                SecurityContextHolder.getContext().setAuthentication(auth);
                request.setAttribute("username", username);
                request.setAttribute("authorities", grantedAuthorities);

            }
        }catch(CustomException e){
            SecurityContextHolder.clearContext();
            response.sendError(e.getHttpStatus().value(), e.getMessage());
            return;
        }
        filterChain.doFilter(request, response);
    }
}
