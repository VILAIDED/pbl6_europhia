package com.pbl6.europhiaauth.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import com.pbl6.europhiaauth.model.User;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    User findByUserName(String username);
    boolean existsByUserName(String username);
}
