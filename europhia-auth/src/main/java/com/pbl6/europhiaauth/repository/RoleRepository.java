package com.pbl6.europhiaauth.repository;

import com.pbl6.europhiaauth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository  extends JpaRepository<Role,Long> {
    Role findByName(String name);


}
