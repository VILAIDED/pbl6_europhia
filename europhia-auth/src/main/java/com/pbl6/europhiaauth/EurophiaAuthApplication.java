package com.pbl6.europhiaauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class EurophiaAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurophiaAuthApplication.class, args);
	}

}
